var next = document.getElementById('next'),
  prev = document.getElementById('back'),
  progressBar = document.getElementById('progressBar'),
  progress = document.querySelector('.progress-bar span'),
  wizardSubmit = document.getElementById('wizardSubmit'),
  wizardPercent = document.getElementById('wizardPercent'),
  resultContainer = document.getElementById('result'),
  result = 0,
  ajaxLoader = document.querySelector('.ajax-loader'),
  resultValue = 0,
  progressWidth = 0,
  counter = 0,
  progressStepLength = 0,
  wizardSection = document.getElementsByClassName('wizard-section');

progressStepLength = Math.round(100 / (wizardSection.length - 1)); //set length of one step for progressbar and percent counter

progress.style.width = progressWidth; //style for progress bar (width = 0)

next.innerHTML = 'Погнали!';

//click on next button
next.addEventListener('click', function (event) {
  event.preventDefault();
  var step = wizardSection[counter].getAttribute('data-step'); //get data-step attribute from current section

  if (step == counter) {
    wizardSection[counter].style.display = 'none'; //hide current section
    wizardSection[counter + 1].style.display = 'block'; //show next section
  }

  counter++;

  console.log(counter);
  progressWidth += progressStepLength;

  if (counter <= 1) {
    progressWidth = 0;
  }

  next.innerHTML = 'Дальше';

  prev.style.display = 'block'; //show next  button

  progressBar.style.display = 'flex';

  progress.style.width = `${progressWidth}%`; //add width to progress bar
  wizardPercent.innerHTML = `${progressWidth}%`; //add percent step for percent counter

  if (counter >= wizardSection.length - 1) {
    next.style.display = 'none';   // hide next and prev buttons if it is the last step
    prev.style.display = 'none';

  }

  if (step == (wizardSection.length - 2)) {

    // Calculate common values all checkboxes and calculate the result
    var inputChecked = document.querySelectorAll('input:checked'), // get all checked radio buttons (it is not an array)
      inputCheckedArray = []; // set empty array

    for (i = 0; i < inputChecked.length; i++) {
      inputCheckedArray[i] = parseInt(inputChecked[i].value); // load values to array
    }

    resultValue = inputCheckedArray.reduce(function (previousValue, currentValue) {
      return previousValue + currentValue;  // get sum of all elements from array
    })

    result = Math.floor(resultValue / (wizardSection.length - 2) * 10) + '%'; // set result value to markup

    resultContainer.innerHTML = result;

    progress.style.width = '100%';
    wizardPercent.innerHTML = '100%';

  }

});

//click on previous button
back.addEventListener('click', function (event) {
  event.preventDefault();
  var step = wizardSection[counter].getAttribute('data-step');

  next.style.display = 'block'; // show next button even if last step was the lasts

  if (step == counter) {
    wizardSection[counter].style.display = 'none';
    wizardSection[counter - 1].style.display = 'block';
  }

  counter--;
  progressWidth -= progressStepLength;

  //set zero progress in first step of quiz, because in fact first step is start page
  if (counter <= 1) {
    progressWidth = 0;
  }

  prev.style.display = 'block';

  progress.style.width = `${progressWidth}%`;
  wizardPercent.innerHTML = `${progressWidth}%`;

  if (counter == 0) {
    prev.style.display = 'none';
    progressBar.style.display = 'none';
    next.innerHTML = 'Start!';
  } else {
    prev.style.display = 'block';
  }

});


//Submit form (use jQuery)

$(function () {
  var form = $('#wizardForm'),
      formMessages = $('#formMessages');

  $(form).submit(function (e) {
    e.preventDefault();

    var formData = {
      result: result, 
      phone: $('#phone').val()
    }; 

    $.ajax({
      type: 'POST',
      url: $(form).attr('action'),
      data: formData,
      success: function (data) {
        console.log('ajax ok ');
      },
      error: function (data) {
        console.log('ajax error ');
      }
    })
      .done(function (response) {
        $(formMessages).toggleClass('cuccess');
        $(formMessages).css('display', 'block');
        $(formMessages).text(response);
        $('#wizardForm input').val('');
      })
      .fail(function (data) {
        $(formMessages).toggleClass('error');
        // $(formMessages).addClass('error');
        $(formMessages).css('display', 'block');
        $(formMessages).text('Упс! Сообщение не удалось отправить');
      });
  });

$(document).ajaxStart(function(){
  $(formMessages).css('display', 'block');
  $('#ajaxLoader').css('display', 'block');
  console.log('ajax-start');
})

});